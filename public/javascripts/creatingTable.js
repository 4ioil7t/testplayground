
function createTable(e){
    e.preventDefault();
	const self = document;
    let mainform = self.getElementById("mainform");
    let fio = mainform["fio"].value;
    let course = mainform["course"].value;
    let fac = mainform["fac"].value;
    let group = mainform["group"].value;
  
    let sub = mainform["sub"].value;
    let teacher = mainform["teacher"].value;
    let edate = mainform["edate"].value;
    let etime = mainform["etime"].value;
    let eform = mainform["eform"].value;
  
    let tdiv = self.getElementById("tableDiv");
    let table = self.getElementById("t1");
    tdiv.style.display="block";
    let row = table.rows;
   
  	row[1].cells[1].innerHTML=fio;
  	row[2].cells[1].innerHTML=course;
    row[3].cells[1].innerHTML=fac;
    row[4].cells[1].innerHTML=group;
    row[5].cells[1].innerHTML=sub;
    row[6].cells[1].innerHTML=teacher;
    row[7].cells[1].innerHTML=edate+" "+etime;
    row[8].cells[1].innerHTML=eform;

  
  return false;
}
